/*Purpose: For drawing cool border inside texts.*/

#include <stdio.h>

int constructTextBorder(const char passedargs[], const int width, const int height)
{
    if(passedargs == NULL) {
        printf("Please pass a argument!\n");
        return 1;
    }

    int TO = 0; /*TextsOutputted*/

    printf("\n");

    for (int x=0; x < width; x++) 
        printf("-");

    for (int y=0; y < height; y++) {
        printf("\n| "); /*next line. add. repeat.*/
	for (int x=0; x < width-3; x++)
	{
		if (passedargs[TO])
		{
			printf("%c", passedargs[TO]);
			TO++;
		}
		else printf(" ");
	} 
	printf(" |");
    }

    printf("\n");

    for (int x=0; x < width; x++) 
	    printf("-");

    printf("\n"); /*end job*/
    
    return 0;
}

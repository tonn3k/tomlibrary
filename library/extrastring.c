/*From tonn3k/mot code*/
int compareString(char * currentString, char * exceptedString)
{
    int i = 0;
    while (currentString[i]==exceptedString[i]) {
        if (currentString[i]==0 && exceptedString[i]==0) {
            return 1;
        }
        i++;
    }
    return 0;
}

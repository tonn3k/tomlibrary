# TomLibrary

A library of simple C functions for easier work.

## Getting started

Just scroll in directory "library" and get what you need.

then include it. example:
```
#Include "TextBorder.h"

main() {
    /*You can see the required arguments in TextBorder.h*/
    constructTextBorder("After long time! I finally have arrived back from France.", 100,50);
    /*text, width, height*/
}

```

## Bugs and problems

Issue it then!

## Contribute

You're welcome to contribute code but there are some requirements and limitations.

1. Code must be clean and simple.
2. No useless codes. (example including another C preinstalled library thats not included in code or adding same code when you could use function)
3. Do not use too much brackets (if if if if while for) than 3.
4. Cannot be over 2000 lines.
5. And... be independent. (no other community library)

## License

No need.

## Progress

Not sure it is finished if there is anything needed but we'll just go with the timeline.
